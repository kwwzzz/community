# openKylin Community
English | [简体中文](./README.md)

Welcome to openKylin Community.

## Introduction

The Community repo stores information about SIG groups that have been created by the openKylin Community.

Welcome to join the SIG you are interested in.  
  
SIG is the abbreviation of Special Interest Group. In order to better manage and improve the workflow, the openKylin community is organized according to different SIGs. Therefore, before making community contributions, you need to find the SIG you are interested in.

## Principle

- 1. All SIG groups in the openKylin community are open, and any person and organization can participate.

- 2. Each SIG group consists of the core members of the project: the project Owner, the maintainer (Maintainer), and the contributor (Conrtibutor). Major decisions in the group must be decided by more than 2/3 of the votes of all members and reported to the technical committee.

- 3. The types of affairs responsible between the SIG groups in the community are not allowed to overlap, and the type of affairs between the groups needs to be kept separate.

- 4. Meetings within each SIG group need to be held regularly.
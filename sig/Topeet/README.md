
# Topeet SIG（北京迅为电子）

Topeet SIG组致力于在嵌入式硬件产品上适配openKylin系统，实现操作系统的智能化和性能优化。

## 工作目标

Topeet SIG组的目标是通过结合嵌入式硬件和Linux操作系统，解决以下关键问题：

- iTOP-RK3568开发板上支持运行openKylin系统，并进行系统优化；

- iTOP-RK3588开发板上支持运行openKylin系统，并进行系统优化。

## SIG 成员

### Owner

- wangdingbang (xunwei_fs@foxmail.com)

### Maintainers

- wangdingbang (xunwei_fs@foxmail.com)

## SIG 维护包列表

- [openkylin_rootfs](https://gitee.com/beijing-xunwei-electronics/openkylin_rootfs)


## SIG 邮件列表

- topeet@lists.openkylin.top

## SIG 组例会

月会

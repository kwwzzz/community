# HygonArch SIG

Hygon Arch SIG主要是围绕海光芯片，将海光C86芯片的架构特点、驱动功能、安全技术、上层软件生态等呈现出来。本SIG将围绕海光处理器的指令集优势，海光内嵌异构安全处理器的安全技术，以及海光C86上层软件应用技术的，结合openKylin社区已有的生态，扩大对芯片的应用。

## 工作目标

- 负责 openKylin HygonArch 版本的规划、制作、维护和升级

## 邮件列表

hygonarch@lists.openkylin.top

## SIG成员

### Owner

- [allen846356](https://gitee.com/allen846356)
- [lcxkevin](https://gitee.com/lcxkevin)
- [fuhao](https://gitee.com/fh87lm)

### Maintainers

- allenli@hygon.cn
- lichangxing@hygon.cn
- fuh@hygon.cn

### Committers

## 仓库列表

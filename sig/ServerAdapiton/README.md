## Server adaption SIG小组

Server adaption SIG为openKylin在服务器平台上的内核及软件适配提供支持.

## 工作目标

1. 整合服务器技术资源及用户生态需求，加速openKylin在服务器操作系统领域发展. 
2. 繁荣openKylin服务器操作系统软件生态，加快共性问题解决.
3. 相关软件工具的联合分享以及合作开发演进.

## SIG成员

## Maintainers
顾剑 (`gujian@phytium.com.cn`)
毛泓博 (`maohongbo@phytium.com.cn`)
帅家坤 (`shuaijiakun1288@phytium.com.cn`)

## SIG交付件列表

- 

## SIG邮件列表
server@lists.openkylin.top

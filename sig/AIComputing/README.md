# 智能计算兴趣小组 (SIG)

AIComputing：智能计算（Artificial Intelligence Computing）

## 工作目标

AIComputing SIG组致力于推动智算GPU与openKylin操作系统的适配，提升系统上运行AI模型的计算效率和准确性，同时保证系统的稳定性和安全性。

## 工作方向

- 智算卡与系统适配：基于openKylin操作系统适配主流的智算卡，构建AIPC底座；
- AI模型训练与推理：在系统上优化AI模型的训练和推理过程，确保数据计算的效率和准确性；
- 算力资源管理：高效管理和分配算力资源，以支持各种AI应用的运行，同时保证系统的高效和稳定；
- 生态系统构建：构建一个健康的AI生态系统，通过提供开发工具和支持，以及促进开源项目的发展。


## SIG 成员

### Owner

- He Jun (jun.he@metax-tech.com)

### Maintainers

- He Jun (jun.he@metax-tech.com)

## SIG 维护包列表



## SIG 邮件列表

- aicomputing@lists.openkylin.top


## SIG 组例会
- 月会

# CompatWinApp兴趣小组（SIG）

CompatWinApp SIG组致力于将大量的Windows系统应用程序引入到openKylin系统。sig组将通过研究应用兼容技术和指令翻译技术，研制完善的windows应用兼容方案，让更多的windows应用能兼容运行于openKylin 系统，不断繁荣openKylin 软件生态。



## 工作目标

- 研制基于OpenKylin系统的windows应用兼容运行环境
- 研制基于OpenKylin系统的二进制翻译方案

## repository
- [compat-winapp](https://gitee.com/openkylin/compat-winapp)
- [wine](https://gitee.com/openkylin/wine)
- [box86](https://gitee.com/openkylin/box86)
- [box64](https://gitee.com/openkylin/box64)
- [win-program](https://gitee.com/openkylin/win-program)
- [win-dependency](https://gitee.com/openkylin/win-dependency)
- [wine-assistant](https://gitee.com/openkylin/wine-assistant)


## 软件包发布：
SIG组发布的openKylin wine助手和wine兼容环境安装包可以如下地址下载：
https://gitee.com/openkylin/compat-winapp/releases

## Bug反馈：
wine助手和wine兼容环境使用过程中的bug，可以反馈至如下网址：
https://gitee.com/openkylin/win-program/issues

## SIG成员
### Maintainers
- liangkeming(liangkeming@kylinos.cn)
- sunshower(liubuquan@kylinos.cn)
- mu_ruichao(muruichao@kylinos.cn)
- xw1985(xuewei@kylinos.cn)
- linchaochao(linchaochao@kylinos.cn)
- chenhaoyang2019(chenhaoyang@kylinos.cn)
- wangchanghe(wangchanghe@kylinos.cn)
- wezheng(weizheng@kylinos.cn)
### Committers


## 邮件列表


# KARE兴趣小组（SIG）
KARE(Kylin Application Runtime Environment) SIG小组致力于完善openkylin系统兼容性

## 工作目标
  
* **应用跨系统版本运行**  
  解决旧软件在新的操作系统上无法正常运行问题

* **兼容多系统版本**  
  兼容centos，ubuntu, openkylin等系统图形应用运行 


## 维护包列表

kare

## SIG 成员

### Owner

- 周华智(zhouhuazhi@kylinos.cn)

### Maintainers

- 张霖(zhanglin@kylinos.cn)


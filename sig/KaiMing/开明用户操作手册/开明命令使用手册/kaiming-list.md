# kaiming 列出所有已安装的应用

``kaiming list`` 命令可以列出所有已安装的应用。

## 用法

查看 ``kaiming list`` 命令的帮助信息：

```
$ kaiming list

```

## 列出所有已安装的应用示例

运行``kaiming list`` 命令可以列出所有已安装的应用。

```bash
$ kaiming list
Name                      Id                                                 Branch               Module     Version              Size       Description
Recorder                  top.openkylin.Recorder                             stable               app        1.3.0.2.0k3.3-ok3.0  4.2 MB     Friendly interface and easy to operate recording tool
Mesa                      org.freedesktop.Platform.GL.default                22.08                runtime    24.0.3               421.7 MB   Mesa - The 3D Graphics Library
Yaru Gtk Theme            org.gtk.Gtk3theme.Yaru                             3.22                 runtime                         727.0 KB   Yaru theme
Mesa (Extra)              org.freedesktop.Platform.GL.default                22.08-extra          runtime    24.0.3               421.7 MB   Mesa - The 3D Graphics Library
Clock                     top.openkylin.Clock                                stable               app        4.0.0.0-ok3.1        15.9 MB    Ukui Alarm
```

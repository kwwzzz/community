# kaiming 查看运行中的应用

``kaiming ps`` 命令可以查看运行中的应用。

## 用法

查看 ``kaiming ps`` 命令的帮助信息：

```
$ kaiming ps

```

## 查看运行中的应用示例

运行``kaiming ps`` 命令可以查看运行中的应用。

```bash
$ kaiming ps # 获取运行中应用信息
instance    pid    app                  runtime
4025818826  27901  top.openkylin.Clock  runtime/top.openkylin.Platform/x86_64/2.0
```

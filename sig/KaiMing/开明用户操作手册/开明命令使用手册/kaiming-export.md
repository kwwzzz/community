# kaiming 离线导出应用

``kaiming export`` 命令可以用来离线导出开明应用。

## 用法

查看 ``kaiming export`` 命令的帮助信息：

```bash
$ sudo kaiming export 自定义导出路径 开明包  # 离线导出开明包

```

## 离线导出仓库示例

运行``kaiming export`` 命令可以用来离线导出开明应用。

```bash
$ sudo kaiming export /home/kylin/kylin-offline/ top.openkylin.Clock # /home/kylin/kylin-offline/ 为导出路径
```

``kaiming export`` 命令需要输入应用完整的 ``appid``。

# kaiming 更新应用

``kaiming update`` 命令可以用来更新开明应用。

查看 ``kaiming update`` 命令的帮助信息：

```bash
$ kaiming update --help
用法：
  kaiming update [选项…] [引用…] - 更新应用程序或运行时

帮助选项：
  -h, --help          显示帮助选项

应用程序选项：
  -y    对所有问题自动回答是
```

## 更新示例

运行 ``kaiming update`` 命令更新开明应用。

```bash
$ kaiming update org.kde.kcalc
```
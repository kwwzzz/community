# 远程调试虚拟机

> 使用ssh调试openkylin虚拟机

## 介绍
在移植DDE桌面环境时，你需要使用虚拟机软件，但是虚拟机的驱动适配并不是很好，有时候复制粘贴代码和传输文档等操作非常麻烦，这时我们可以使用ssh来帮助我们

## 虚拟机的配置工作

首先你需要确保虚拟机正确连接了虚拟网卡，在连接模式中选择桥接模式，在终端中输入以下命令安装openssh-server并开启服务

```bash
sudo apt update
sudo apt install openssh-server -y

//启动服务（需要输入密码，如图）
systemctl start ssh

//开机自启动服务
systemctl enable ssh
```

![配置ssh](image-3.png)

接下来我们需要知道虚拟机的IP地址，输入ifconfig命令得到网络信息

![获取ip](image.png)

例如这台主机的地址就是 192.168.0.111，可以先使用ping看看能不能正常连接

```bash
ping xxx.xxx.x.xxx
```

![ping](image-1.png)

## 使用VSCode连接

首先在VSCode安装 Remote - SSH 插件，重启VSCode并配置ssh连接

```shell
# Read more about SSH config files: https://linux.die.net/man/5/ssh_config
Host openkylin
    HostName 192.168.0.111
    User DSOE1024
```

连接虚拟机，你可以远程访问虚拟机的目录和终端

访问文件：
![vsssh](image-2.png)

终端：
![Alt text](image-4.png)

这样，就可以更方便进行开发工作
# Realtek Special Interest Group (SIG)

Realtek-SIG: Realtek-SIG will focus on enabling Realtek Wi-Fi in openKylin OS.

# objectectives:
- Enabling Realtek Wi-Fi in openKylin OS.
Realtek will continually release new Wi-Fi driver including PCIE, USB and SDIO based on customer request. 

## SIG  members

### Owner
- qingxinsun (qingxin_sun@realsil.com.cn)

### Maintainers
- qingxinsun (qingxin_sun@realsil.com.cn)


### Realtek SIG packages
- realtek-wifi 

### Realtek SIG mail list
- 


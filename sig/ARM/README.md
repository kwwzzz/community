## ARM SIG

ARM SIG致力于ARM架构生态的共建和推广，以及基于openKylin内核支持ARM Vendors平台。

## 工作目标

1. 加快Arm架构共性问题在上游社区的问题解决，或从上游社区backport特性以解决openKylin Kernel问题。
2. 收集Arm架构用户及厂商需求，完善openKylin Kernel Arm 生态。
3. 相关迁移工具、优化工具的联合分享以及合作开发演进。

## SIG成员

## Maintainers
gookevin2019 (`gujian@phytium.com.cn`)
mao-hongbo (`maohongbo@phytium.com.cn`)
shuaijiakun (`shuaijiakun1288@phytium.com.cn`)
mr-liubaggio (`liuyh62@lenovo.com`)

## SIG交付件列表

- 

## SIG邮件列表
arm@lists.openkylin.top

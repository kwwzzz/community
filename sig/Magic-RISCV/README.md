# MAGIC-RISCV SIG

我们目前正积极开发一套全面的RISC-V架构适配解决方案，旨在为开发者们提供包括编译报错分析工具、源代码定位工具等辅助适配工具在内的一系列资源。这套解决方案将覆盖软件的迁移开发周期，从编码、编译、构建到系统改造的各个关键步骤。

## 工作目标

- 开发维护 MAGIC-RISCV 辅助适配工具。

## 邮件列表

<magicProduct@jinwang.com.cn>

## SIG成员

### Owner

- [jwshenct](https://gitee.com/shenchengtao)

### Maintainers

- [Xn](https://gitee.com/codexn)
- [cxl](https://gitee.com/cxcxl)
- [Gyangustar233](https://gitee.com/gyangustar233)
- [GPLer](https://gitee.com/GPLer)
- [Jero丶泽](https://gitee.com/luosz008)

## 仓库列表

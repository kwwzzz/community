# OS4AIPC (SIG)

OS4AIPC：人工智能个人计算机操作系统（Operating System for Artificial Intelligence Personal Computer）致力于将人工智能（AI）与桌面操作系统（OS）深度融合，特别强调端侧大模型和本地推理能力。通过将大模型为代表的AI技术融入openKylin操作系统，并提供统一的AI接口能力，与桌面环境无缝集成，实现包括AI助手、智能文生图、智能模糊搜索等在内的一系列智能化功能，以提升用户在国产桌面操作系统上的办公效率和创作体验。

## 工作目标

OS4AIPC的目标是通过深度融合AI和个人计算机操作系统，解决以下关键问题：

- 安全性与隐私保护：基于端侧模型和本地推理的AI技术，确保用户数据的安全和隐私，无需依赖云端。

- 模型压缩：通过模型压缩技术，降低大模型的存储和计算资源需求，使其在端侧设备上高效运行，并保持足够性能。

- 高效推理：优化的计算资源管理和调度系统，加速本地大模型推理，确保复杂AI任务的高效执行，减少对网络的依赖。

- 性能优化：利用端侧模型和AI技术来提高操作系统的性能，减少资源占用和响应时间，以更高效地运行应用程序，实现低延迟用户体验。

- 用户体验改进：通过智能化的界面和功能，提供更流畅、个性化和用户友好的操作系统体验。

- 开发者支持：提供统一的AI接口能力，帮助开发者快速定制和部署基于端侧模型和本地推理的AI应用程序，加速开发和部署周期。

## SIG 成员

### Owner

- yujie (yujie@yhkylin.cn)

- liuxiaodong (liuxiaodong@yhkylin.cn)

### Maintainers

- penglong(penglong@yhkylin.cn)
- jibin(jibin@yhkylin.cn)
- lizhuo(lizhuo@yhkylin.cn)
- lihanhua (lihanhua@yhkylin.cn)
- wurou(wurou@yhkylin.cn)
- huangjiangjie(huangjiangjie@yhkylin.cn)
- lilinbo(lilinbo@yhkylin.cn)

## SIG 维护包列表

- [llm-agent](https://gitee.com/openkylin/llm-agent)

- [chat-client](https://gitee.com/openkylin/chat-client)

- [kylin-actuator](https://gitee.com/openkylin/kylin-actuator)

- [audio-text](https://gitee.com/openkylin/audio-text)

- [tts-ffi](https://gitee.com/openkylin/tts-ffi)

- [stt-ffi](https://gitee.com/openkylin/stt-ffi)

## SIG 邮件列表

- [os4aipc@lists.openkylin.top](mailto:os4aipc@lists.openkylin.top)

## SIG 组例会

月会
